const list = document.querySelector('.display');
const ulFilms = document.createElement('ul');
ulFilms.classList.add('ul-content');

async function data(url = '') {
    const response = await fetch(url);

    const data = await response.json();

    data.forEach(item => {
        const film = document.createElement('li');
        film.classList.add('li-item');

        const persons = document.createElement('ul');
        persons.classList.add('ul-item');
        item.characters.forEach(person => {
            fetch(person)
                .then(person => person.json())
                .then(person => person.name)
                .then(name => {
                    const person = document.createElement('li');
                    person.textContent = name;
                    persons.append(person);
                });
        });

        film.innerHTML = `<strong>Фільм</strong> -> ${item.name},<br/> 
        ЕПІЗОД - ${item.episodeId} 
        <br/> Короткий зміст: ${item.openingCrawl}
        <br/> <strong>Cписок персонажів</strong>
        <br/>`;
        film.append(persons);
        ulFilms.append(film);
    });
    list.classList.add('loaded_hiding');

    window.setTimeout(function () {
        list.classList.add('loaded');
        list.classList.remove('loaded_hiding');
    }, 300);
}

data('https://ajax.test-danit.com/api/swapi/films');

list.append(ulFilms);
